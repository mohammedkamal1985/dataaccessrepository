﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Dapper;
using Repository.Base.Interfaces;

namespace Repository.Base.Sql.Dapper.Abstract
{
    public abstract class QueryRepositoryBase<DBType, DomainIdType, DbId> : IQueryRepository
    {
        protected IDbConnection _connection;
        protected string _getByIdProcedure;
        protected string _getAllProcedure;

        public QueryRepositoryBase(IDbConnection connection)
        {
            _connection = connection;
        }

        #region GET
        public virtual IEnumerable<TEntity> Get<TEntity>(
            Expression<Func<TEntity, bool>> filter, 
            Func<IQueryable<TEntity>,IOrderedQueryable<TEntity>> orderBy,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> includeProperties,
            int? skip,
            int? take) where TEntity : class, IDBEntityBase
        {
            try
            {
                var query = _connection.Query<TEntity>(_getAllProcedure, null, null, true, null, CommandType.StoredProcedure).AsQueryable();

                if (filter != null)
                {
                    query = query.Where(filter);
                }

                if (orderBy != null)
                {
                    query = orderBy(query);
                }

                if (skip.HasValue)
                {
                    query = query.Skip(skip.Value);
                }

                if (take.HasValue)
                {
                    query = query.Take(take.Value);
                }

                return query;
            }
            catch (DbException sqlEx)
            {
                //TODO LOG & Handle DatabaseException
                throw;
            }
            catch (Exception ex)
            {
                //TODO LOG & Handle DatabaseException
                throw;
            }
        }

        public virtual Task<IEnumerable<TEntity>> GetAsync<TEntity>(
            Expression<Func<TEntity, bool>> filter,
            Func<IQueryable<TEntity>,IOrderedQueryable<TEntity>> orderBy,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> includeProperties,
            int? skip,
            int? take) where TEntity : class, IDBEntityBase
        {
            try
            {
                return new Task<IEnumerable<TEntity>>(() =>
                {
                    var query = _connection.QueryAsync<TEntity>(_getAllProcedure, null, null, null, CommandType.StoredProcedure).Result.AsQueryable();

                    if (filter != null)
                    {
                        query = query.Where(filter);
                    }

                    if (orderBy != null)
                    {
                        query = orderBy(query);
                    }

                    if (skip.HasValue)
                    {
                        query = query.Skip(skip.Value);
                    }

                    if (take.HasValue)
                    {
                        query = query.Take(take.Value);
                    }
                    return query;
                });
            }
            catch (DbException sqlEx)
            {
                //TODO LOG & Handle DatabaseException
                throw;
            }
            catch (Exception ex)
            {
                //TODO LOG & Handle DatabaseException
                throw;
            }
        }

        #endregion

        #region GETALL
        public virtual IEnumerable<TEntity> GetAll<TEntity>(
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> includeProperties,
            int? skip,
            int? take) where TEntity : class, IDBEntityBase
        {
            try
            {
                return this.Get<TEntity>(null, orderBy, includeProperties, skip, take);
            }
            catch (DbException sqlEx)
            {
                //TODO LOG & Handle DatabaseException
                throw;
            }
            catch (Exception ex)
            {
                //TODO LOG & Handle DatabaseException
                throw;
            }
        }

        public virtual Task<IEnumerable<TEntity>> GetAllAsync<TEntity>(
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> includeProperties,
            int? skip,
            int? take) where TEntity : class, IDBEntityBase
        {
            try
            {
                return this.GetAsync<TEntity>(null, orderBy, includeProperties, skip, take);
            }
            catch (DbException sqlEx)
            {
                //TODO LOG & Handle DatabaseException
                throw;
            }
            catch (Exception ex)
            {
                //TODO LOG & Handle DatabaseException
                throw;
            }
        }
        #endregion

        #region GET_BY_ID
        public virtual TEntity GetById<TEntity>(object id
        ,Func<IQueryable<TEntity>, IQueryable<TEntity>> includeProperties) 
        where TEntity : class, IDBEntityBase
        {
            try
            {
                return _connection.QueryFirstOrDefault<TEntity>(_getByIdProcedure, new { Id = id }, null, null, CommandType.StoredProcedure);
            }
            catch (DbException sqlEx)
            {
                //TODO LOG & Handle DatabaseException
                throw;
            }
            catch (Exception ex)
            {
                //TODO LOG & Handle DatabaseException
                throw;
            }
        }

        public virtual Task<TEntity> GetByIdAsync<TEntity>(object id
        ,Func<IQueryable<TEntity>, IQueryable<TEntity>> includeProperties) 
        where TEntity : class, IDBEntityBase
        {
            try
            {
                return _connection.QueryFirstOrDefaultAsync<TEntity>(_getByIdProcedure, new { Id = id }, null, null, CommandType.StoredProcedure);
            }
            catch (DbException sqlEx)
            {
                //TODO LOG & Handle DatabaseException
                throw;
            }
            catch (Exception ex)
            {
                //TODO LOG & Handle DatabaseException
                throw;
            }
        }
        #endregion

        #region GET_COUNT
        public virtual int GetCount<TEntity>(Expression<Func<TEntity, bool>> filter) where TEntity : class, IDBEntityBase
        {
            try
            {
                var query = _connection.Query<TEntity>(_getAllProcedure, null, null, true, null, CommandType.StoredProcedure).AsQueryable();

                if (filter != null)
                {
                    query = query.Where(filter);
                }

                return query.Count();
            }
            catch (DbException sqlEx)
            {
                //TODO LOG & Handle DatabaseException
                throw;
            }
            catch (Exception ex)
            {
                //TODO LOG & Handle DatabaseException
                throw;
            }
        }

        public virtual Task<int> GetCountAsync<TEntity>(Expression<Func<TEntity, bool>> filter) where TEntity : class, IDBEntityBase
        {
            try
            {
                return new Task<int>(() =>
                {
                    var query = _connection.QueryAsync<TEntity>(_getAllProcedure, null, null, null, CommandType.StoredProcedure).Result.AsQueryable();

                    if (filter != null)
                    {
                        query = query.Where(filter);
                    }

                    return query.Count();
                });
            }
            catch (DbException sqlEx)
            {
                //TODO LOG & Handle DatabaseException
                throw;
            }
            catch (Exception ex)
            {
                //TODO LOG & Handle DatabaseException
                throw;
            }
        }
        #endregion


        /*
public DBType Find(DbId id)
{
   try
   {
       return _connection.QueryFirstOrDefault<DBType>(_getByIdProcedure, new { Id = id }, null, null, CommandType.StoredProcedure);
   }
   catch (DbException sqlEx)
   {
       //TODO LOG & Handle DatabaseException
       throw;
   }
   catch (Exception ex)
   {
       //TODO LOG & Handle DatabaseException
       throw;
   }
}

public virtual DBType FindByDomainId(DomainIdType domainId)
{
   try
   {
      return _connection.QueryFirstOrDefault<DBType>(_getByDomainIdProcedure, new { DomainId = domainId },null,null, CommandType.StoredProcedure);
   }
   catch(DbException sqlEx)
   {
       //TODO LOG & Handle DatabaseException
       throw;
   }
   catch (Exception ex)
   {
       //TODO LOG & Handle DatabaseException
       throw;
   }
}

public virtual IEnumerable<DBType> List()
{
   try
   {
       return _connection.Query<DBType>(_getAllProcedure, null,null,true, null, CommandType.StoredProcedure);
   }
   catch (DbException sqlEx)
   {
       //TODO LOG & Handle DatabaseException
       throw;
   }
   catch (Exception ex)
   {
       //TODO LOG & Handle DatabaseException
       throw;
   }
}
*/
    }
}
