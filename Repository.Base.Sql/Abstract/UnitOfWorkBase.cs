﻿using System.Data;
using Repository.Base.Interfaces;

namespace Repository.Base.Sql.Dapper.Abstract
{
    public class UnitOfWorkBase : IUnitOfWork
    {
        protected IDbConnection _connection;
        public UnitOfWorkBase(IDbConnection connection)
        {
            _connection = connection;
        }
        public void CloseConnection()
        {
           if(_connection.State == ConnectionState.Closed)
            {
                return;
            }
            _connection.Close();
        }

        public void OpenConnection()
        {
            if (_connection.State == ConnectionState.Open)
            {
                return;
            }
            _connection.Open();
        }

        public void SaveChanges()
        {
            throw new System.NotImplementedException();
        }
    }
}
