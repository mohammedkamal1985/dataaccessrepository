﻿using System;
using System.Data;
using System.Data.Common;
using Dapper;
using Repository.Base.Interfaces;


namespace Repository.Base.Sql.Dapper.Abstract
{
    public abstract class WriteRepositoryBase<DBType> : IWriteRepository<DBType>
    {
        private IDbConnection _connection;
        protected string INSERT_PROCEDURE;
        protected string UPDATE_PROCEDURE;
        protected string DELETE_PROCEDURE;


        public WriteRepositoryBase(IDbConnection connection)
        {
            _connection = connection;
        }

        protected abstract DynamicParameters CreateInsertParameters(DBType entity);
        public virtual DBType Add(DBType entity)
        {
            try
            {
                return _connection.ExecuteScalar<DBType>(INSERT_PROCEDURE, CreateInsertParameters(entity), null, null, CommandType.StoredProcedure);
            }
            catch (DbException sqlException)
            {
                //TODO Log and Handle Exception 
                throw sqlException;
            }
            catch (Exception exception)
            {
                //TODO Log and Handle Exception 
                throw exception;
            }
        }

        public abstract void Delete(DBType entity);
       
        protected abstract DynamicParameters CreateUpdateParameters(DBType entity);
        public DBType Update(DBType entity)
        {
            try
            {
                return _connection.ExecuteScalar<DBType>(UPDATE_PROCEDURE, CreateUpdateParameters(entity), null, null, CommandType.StoredProcedure);
            }
            catch (DbException sqlException)
            {
                //TODO Log and Handle Exception 
                throw sqlException;
            }
            catch (Exception exception)
            {
                //TODO Log and Handle Exception 
                throw exception;
            }
        }
    }
}
