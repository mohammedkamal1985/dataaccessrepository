﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Base.Interfaces;

namespace Repository.Base.Sql.Dapper.Entities
{
    public class DBEntityBase<DomainIdType> : DBEntityBase
    {
        //[Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        //public int Id { get; set; }

        public DomainIdType DomainId { get; set; }

        //public override bool Equals(object entity)
        //{
        //    return entity != null
        //       && entity is DBEntityBase<DomainIdType>
        //       && this == (DBEntityBase<DomainIdType>)entity;
        //}

        //public override int GetHashCode()
        //{
        //    return this.Id.GetHashCode();
        //}

        //public bool Equals(DBEntityBase<DomainIdType> other)
        //{
        //    if (other == null)
        //    {
        //        return false;
        //    }
        //    return this.Id.Equals(other.Id);
        //}

        //public static bool operator ==(DBEntityBase<DomainIdType> entity1, DBEntityBase<DomainIdType> entity2)
        //{
        //    if ((object)entity1 == null && (object)entity2 == null)
        //    {
        //        return true;
        //    }

        //    if ((object)entity1 == null || (object)entity2 == null)
        //    {
        //        return false;
        //    }

        //    if (entity1.Id.ToString() == entity2.Id.ToString())
        //    {
        //        return true;
        //    }

        //    return false;
        //}

        //public static bool operator !=(DBEntityBase<DomainIdType> entity1, DBEntityBase<DomainIdType> entity2)
        //{
        //    return (!(entity1 == entity2));
        //}

    }

    public abstract class DBEntityBase : IDBEntityBase, IEquatable<DBEntityBase>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public override bool Equals(object entity)
        {
            return entity != null
               && entity is DBEntityBase
               && this == (DBEntityBase)entity;
        }

        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        public bool Equals(DBEntityBase other)
        {
            if (other == null)
            {
                return false;
            }
            return this.Id.Equals(other.Id);
        }

        public static bool operator ==(DBEntityBase entity1, DBEntityBase entity2)
        {
            if ((object)entity1 == null && (object)entity2 == null)
            {
                return true;
            }

            if ((object)entity1 == null || (object)entity2 == null)
            {
                return false;
            }

            if (entity1.Id.ToString() == entity2.Id.ToString())
            {
                return true;
            }

            return false;
        }

        public static bool operator !=(DBEntityBase entity1, DBEntityBase entity2)
        {
            return (!(entity1 == entity2));
        }

    }
}
