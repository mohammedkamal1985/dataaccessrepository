﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Expressions;

namespace Repository.Base.Interfaces
{
    public interface IQueryRepository//<DataModelType, DomainIdType,DbId>
    {
        IEnumerable<TEntity> GetAll<TEntity>(
        Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
        Func<IQueryable<TEntity>, IQueryable<TEntity>> includeProperties = null,
        int? skip = null,
        int? take = null)
        where TEntity : class, IDBEntityBase;

        Task<IEnumerable<TEntity>> GetAllAsync<TEntity>(
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> includeProperties = null,
            int? skip = null,
            int? take = null)
            where TEntity : class, IDBEntityBase;

        IEnumerable<TEntity> Get<TEntity>(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> includeProperties = null,
            int? skip = null,
            int? take = null)
            where TEntity : class, IDBEntityBase;

        Task<IEnumerable<TEntity>> GetAsync<TEntity>(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> includeProperties = null,
            int? skip = null,
            int? take = null)
            where TEntity : class, IDBEntityBase;
        
        TEntity GetById<TEntity>(object id,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> includeProperties=null)
            where TEntity : class, IDBEntityBase;

        Task<TEntity> GetByIdAsync<TEntity>(object id,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> includeProperties=null)
            where TEntity : class, IDBEntityBase;

        int GetCount<TEntity>(Expression<Func<TEntity, bool>> filter = null)
            where TEntity : class, IDBEntityBase;

        Task<int> GetCountAsync<TEntity>(Expression<Func<TEntity, bool>> filter = null)
            where TEntity : class, IDBEntityBase;
        
    }
}
