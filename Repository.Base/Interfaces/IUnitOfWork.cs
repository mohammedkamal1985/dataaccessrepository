﻿namespace Repository.Base.Interfaces
{
    public interface IUnitOfWork
    {
        void OpenConnection();
        void CloseConnection();
        void SaveChanges();
    }
}
