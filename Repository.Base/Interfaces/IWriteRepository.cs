﻿
namespace Repository.Base.Interfaces
{
    public interface IWriteRepository<TEntity>
    {
        TEntity Update(TEntity entity) ;
        TEntity Add(TEntity entity) ;
        void Delete(TEntity entity) ;
    }
}
