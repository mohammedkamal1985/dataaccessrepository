﻿using Repository.Base.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repository.Base.Sql.EntityFramework.Entities

{

    public class DBEntityBase<DomainIdType> : DBEntityBase
    {
        public DomainIdType Id { get; set; }
    }

    public abstract class DBEntityBase : IDBEntityBase, IEquatable<DBEntityBase>
    {
        public int DatabaseId { get; set; }

        public override bool Equals(object entity)
        {
            return entity != null
               && entity is DBEntityBase
               && this == (DBEntityBase)entity;
        }

        public override int GetHashCode()
        {
            return this.DatabaseId.GetHashCode();
        }

        public bool Equals(DBEntityBase other)
        {
            if (other == null)
            {
                return false;
            }
            return this.DatabaseId.Equals(other.DatabaseId);
        }

        public static bool operator ==(DBEntityBase entity1, DBEntityBase entity2)
        {
            if ((object)entity1 == null && (object)entity2 == null)
            {
                return true;
            }

            if ((object)entity1 == null || (object)entity2 == null)
            {
                return false;
            }

            if (entity1.DatabaseId.ToString() == entity2.DatabaseId.ToString())
            {
                return true;
            }

            return false;
        }

        public static bool operator !=(DBEntityBase entity1, DBEntityBase entity2)
        {
            return (!(entity1 == entity2));
        }

    }
}
