﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Repository.Base.Sql.EntityFramework.Entities;
using Repository.Base.Interfaces;
using Repository.Base.Abstract;

namespace Repository.Base.Sql.EntityFramework.Abstract
{
    public abstract class RepositoryBase<DbContextType, DataModelType, MetadataType, DomainIdType> : IQueryRepository<DataModelType, DomainIdType, int>,
        IAddAndModifyRepository<DataModelType>
        where DataModelType : DBEntityBase
        where MetadataType:MetadataBase
        where DbContextType : DbContext
    {
        protected DbContextType _context;
        public RepositoryBase(DbContextType context)
        {
            _context = context;
        }

        public virtual DataModelType Add(DataModelType entity)
        {
            try
            {
                var md = ConvertDataModelToMetaData(entity);
                return ConvertMetaDataToDataModel(_context.Set<MetadataType>().Add(md).Entity);
            }
            catch (DbException sqlEx)
            {
                //TODO LOG & Handle DatabaseException
                throw;
            }
            catch (Exception ex)
            {
                //TODO LOG & Handle DatabaseException
                throw;
            }
        }

        public virtual void Delete(DataModelType entity)
        {
            try
            {
                var md = ConvertDataModelToMetaData(entity);
                _context.Set<MetadataType>().Remove(md);
            }
            catch (DbException sqlEx)
            {
                //TODO LOG & Handle DatabaseException
                throw;
            }
            catch (Exception ex)
            {
                //TODO LOG & Handle DatabaseException
                throw;
            }
        }

        public virtual DataModelType Update(DataModelType entity)
        {
            try
            {
                var metadata = ConvertDataModelToMetaData(entity);
                var current = _context.Set<MetadataType>().SingleOrDefault(e => e.Id == entity.Id);
                foreach (PropertyInfo p in typeof(MetadataType).GetProperties())
                {
                    current.GetType().GetProperty(p.Name).SetValue(current, metadata.GetType().GetProperty(p.Name).GetValue(metadata));
                }

                //_context.Entry<MetadataType>(current).State = EntityState.Modified;
                return ConvertMetaDataToDataModel(current);
            }
            catch (DbException sqlEx)
            {
                //TODO LOG & Handle DatabaseException
                throw;
            }
            catch (Exception ex)
            {
                //TODO LOG & Handle DatabaseException
                throw;
            }
        }
        
        public virtual DataModelType Find(int id)
        {
            try
            {
                return _context.Set<DataModelType>().SingleOrDefault(e => e.Id == id);
            }
            catch (DbException sqlEx)
            {
                //TODO LOG & Handle DatabaseException
                throw;
            }
            catch (Exception ex)
            {
                //TODO LOG & Handle DatabaseException
                throw;
            }
        }

        public abstract DataModelType FindByDomainId(DomainIdType domainId);

        public virtual IEnumerable<DataModelType> List()
        {
            try
            {
                return _context.Set<DataModelType>().ToList();
            }
            catch (DbException sqlEx)
            {
                //TODO LOG & Handle DatabaseException
                throw;
            }
            catch (Exception ex)
            {
                //TODO LOG & Handle DatabaseException
                throw;
            }
        }

        protected abstract MetadataType ConvertDataModelToMetaData(DataModelType model);
        protected abstract DataModelType ConvertMetaDataToDataModel(MetadataType metadata);
    }
}
