﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Repository.Base.Interfaces;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Data.Common;

namespace Repository.Base.Sql.EntityFramework.Abstract
{
    public abstract class QueryRepositoryBase<DbContextType> : IQueryRepository
        where DbContextType : DbContext
    {
        protected DbContextType _context;
        public QueryRepositoryBase(DbContextType context)
        {
            _context = context;
        }

        protected virtual IQueryable<TEntity> GetQueryable<TEntity>(
        Expression<Func<TEntity, bool>> filter = null,
        Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
        Func<IQueryable<TEntity>, IQueryable<TEntity>> IncludedProperties = null,
        int? skip = null,
        int? take = null)
        where TEntity : class, IDBEntityBase
        {
            IQueryable<TEntity> query = _context.Set<TEntity>();

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (IncludedProperties != null)
            {
                query = IncludedProperties(query);
            }

            if (orderBy != null)
            {
                query = orderBy(query);
            }

            if (skip.HasValue)
            {
                query = query.Skip(skip.Value);
            }

            if (take.HasValue)
            {
                query = query.Take(take.Value);
            }

            return query;
        }

        public IEnumerable<TEntity> Get<TEntity>(
            Expression<Func<TEntity, bool>> filter,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> includeProperties,
            int? skip,
            int? take)
            where TEntity : class, IDBEntityBase
        {
            return GetQueryable(filter, orderBy, includeProperties, skip, take).ToList();
        }

        public IEnumerable<TEntity> GetAll<TEntity>(
            Func<IQueryable<TEntity>,
            IOrderedQueryable<TEntity>> orderBy,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> includeProperties,
            int? skip,
            int? take)
         where TEntity : class, IDBEntityBase
        {
            return GetQueryable(null, orderBy, includeProperties, skip, take).ToList();
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync<TEntity>(
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> includeProperties,
            int? skip,
            int? take)
         where TEntity : class, IDBEntityBase
        {
            return await GetQueryable<TEntity>(null, orderBy, includeProperties, skip, take).ToListAsync();
        }

        public async Task<IEnumerable<TEntity>> GetAsync<TEntity>(
            Expression<Func<TEntity, bool>> filter,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> includeProperties,
            int? skip,
            int? take)
        where TEntity : class, IDBEntityBase
        {
            return await GetQueryable<TEntity>(filter, orderBy, includeProperties, skip, take).ToListAsync();
        }

        public TEntity GetById<TEntity>(object id,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> includeProperties)
        where TEntity : class, IDBEntityBase
        {
            return _context.Set<TEntity>().Find(id);
        }

        public async Task<TEntity> GetByIdAsync<TEntity>(object id,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> includeProperties)
        where TEntity : class, IDBEntityBase
        {
            return await _context.Set<TEntity>().FindAsync(id);
        }

        public int GetCount<TEntity>(Expression<Func<TEntity, bool>> filter) where TEntity : class, IDBEntityBase
        {
            return GetQueryable<TEntity>(filter).Count();
        }

        public async Task<int> GetCountAsync<TEntity>(Expression<Func<TEntity, bool>> filter) where TEntity : class, IDBEntityBase
        {
            return await GetQueryable<TEntity>(filter).CountAsync();
        }

    }
}
