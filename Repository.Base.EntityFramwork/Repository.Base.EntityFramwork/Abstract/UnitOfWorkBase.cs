﻿using System;
using Microsoft.EntityFrameworkCore;
using Repository.Base.Interfaces;

namespace Repository.Base.Sql.EntityFramework.Abstract
{
    public class UnitOfWorkBase<DbContextType> : IUnitOfWork where DbContextType:DbContext
    {
        DbContextType _context;

        public UnitOfWorkBase(DbContextType context)
        {
            _context = context;
        }
        public void CloseConnection()
        {
            _context.Dispose();
        }

        public void OpenConnection()
        {
            _context = Activator.CreateInstance<DbContextType>();
        }

        public virtual void SaveChanges()
        {
            _context.SaveChanges();
        }
    }
}
