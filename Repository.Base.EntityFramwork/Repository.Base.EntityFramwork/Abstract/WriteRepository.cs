﻿using System;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Repository.Base.Interfaces;
using Repository.Base.Abstract;
using System.Collections;

namespace Repository.Base.Sql.EntityFramework.Abstract
{
    public abstract class WriteRepositoryBase<DbContextType, MetadataType, TEntity> : IWriteRepository<TEntity>
        where TEntity : class, IDBEntityBase
        where MetadataType : MetadataBase
        where DbContextType : DbContext
    {
        private DbContextType _context;
        public WriteRepositoryBase(DbContextType context)
        {
            _context = context;
        }

        public virtual TEntity Add(TEntity entity)
        {
            var md = ConvertDataModelToMetaData(entity);
            var newEntity = _context.Set<MetadataType>().Add(md).Entity;
            return ConvertMetaDataToDataModel(newEntity);
        }

        public virtual void Delete(TEntity entity)
        {
            var md = ConvertDataModelToMetaData(entity);
            if (_context.Entry(entity).State == EntityState.Detached)
            {
                _context.Set<TEntity>().Attach(entity);
            }
            _context.Set<TEntity>().Remove(entity);
        }

        public virtual TEntity Update(TEntity entity)
        {
            var modifiedMetatdata = ConvertDataModelToMetaData(entity);
            var current = _context.Set<MetadataType>().SingleOrDefault(e => e.Id == modifiedMetatdata.Id);
            _context.Entry<MetadataType>(current).CurrentValues.SetValues(modifiedMetatdata);
            //_context.Set<MetadataType>().Update(current);
            return ConvertMetaDataToDataModel(current);
        }

        protected abstract MetadataType ConvertDataModelToMetaData(TEntity entity);
        protected abstract TEntity ConvertMetaDataToDataModel(MetadataType metadata);
    }
}
